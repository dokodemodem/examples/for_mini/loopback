/*
 * Sample program for DokodeModem
 * loopback for modem
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>

DOKODEMO Dm = DOKODEMO();

void setup()
{
  Dm.begin(); // 初期化が必要です。

  UartModem.begin(19200); // 通信速度を設定します。特定小電力無線モデムのUART速度は19200bps
  SerialDebug.begin(115200);

  Dm.ModemPowerCtrl(ON);  // 内蔵モデムの電源をいれます
}

void loop()
{
  // USBデバッグからのデータをモデム側に送信します
  while (SerialDebug.available())
  {
    uint8_t data = SerialDebug.read();
    // SerialDebug.write(data); //echo back
    UartModem.write(data);
  }

  // モデムからのデータをUSBデバッグ側に送信します
  while (UartModem.available())
  {
    SerialDebug.write(UartModem.read());
  }
}
